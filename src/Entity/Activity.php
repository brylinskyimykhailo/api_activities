<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActivityRepository")
 */
class Activity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $popular;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActivityImagesLink", mappedBy="activity")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ActivityCategory", inversedBy="activities")
     * @ORM\JoinTable(name="activity_category_link",
     *     joinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *     )
     */
    private $categories;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPopular(): ?int
    {
        return $this->popular;
    }

    public function setPopular(int $popular): self
    {
        $this->popular = $popular;

        return $this;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }
}
