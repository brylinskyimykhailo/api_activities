<?php

namespace App\Controller;

use App\Repository\ActivityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ActivitiesController extends AbstractController
{
    public function activities(
        ActivityRepository $activityRepository,
        ?int $popular,
        ?int $limit,
        ?int $offset,
        string $direction = 'asc'
    ) {
        if (!empty($direction)) {
            $orderBy = ['price' => $direction];
        }

        if (isset($popular)) {
            $criteria = ["popular" => $popular];
        }

        $activities = $activityRepository->findBy($criteria ?? [], $orderBy ?? null, $limit, $offset);

        return $this->json($this->getStructuredData($activities));
    }

    public function activitiesByCategory(
        ActivityRepository $activityRepository,
        string $name,
        ?int $limit,
        ?int $offset,
        string $direction = 'asc'
    ) {
        $activities = $activityRepository->findByCategory($name, $limit, $offset, $direction);

        return $this->json($this->getStructuredData($activities));
    }

    public function activitiesUntilPrice(
        ActivityRepository $activityRepository,
        int $price,
        ?int $limit,
        ?int $offset,
        string $direction = 'asc'
    ) {
        $activities = $activityRepository->findUntilPrice($price, $limit, $offset, $direction);

        return $this->json($this->getStructuredData($activities));
    }

    private function getStructuredData(array $activities): array
    {
        $structuredActivity = [];

        foreach ($activities as $activity) {
            $activityId = $activity->getId();
            $activityItem = [
                'name'        => $activity->getName(),
                'description' => $activity->getDescription(),
                'popular'     => (bool) $activity->getPopular(),
                'price'       => $activity->getPrice(),
                'images'      => [],
                'categories'  => [],
            ];

            foreach ($activity->getImages() as $imageLink) {
                $activityItem['images'][] = $imageLink->getImageUrl();
            }

            foreach ($activity->getCategories() as $category) {
                $activityItem['categories'][] = $category->getName();
            }

            $structuredActivity[$activityId] = $activityItem;
        }

        return ['activities' => $structuredActivity];
    }
}
