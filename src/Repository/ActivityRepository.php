<?php

namespace App\Repository;

use App\Entity\Activity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Activity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activity[]    findAll()
 * @method Activity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Activity::class);
    }

    public function findByCategory(string $name, ?int $limit, ?int $offset, string $direction)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->leftJoin('a.categories', 'c')
            ->andWhere('c.name = :name')
            ->setParameter('name', $name)
            ->orderBy('a.price', $direction)
        ;

        if (isset($limit) && isset($offset)) {
            $queryBuilder->setFirstResult($offset)->setMaxResults($limit);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findUntilPrice(int $maxPrice, ?int $limit, ?int $offset, string $direction)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->andWhere('a.price < :max_price')
            ->setParameter('max_price', $maxPrice)
            ->orderBy('a.price', $direction)
        ;

        if (isset($limit) && isset($offset)) {
            $queryBuilder->setFirstResult($offset)->setMaxResults($limit);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
