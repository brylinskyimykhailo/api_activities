#### **Simple JSON API that work with mysql DB to get data and return collection of results in json format**

##### A project based on Symfony 4.4

Installation
========================

1. Clone or download repository

2. Run composer

	    composer install

3. Set configuration in .env and create DB
        
        php bin/console doctrine:database:create

4. Execute the migration

        php bin/console make:migration
        
5. Run server
    
        php -S localhost:8000 -t public/
